package chat;

import java.rmi.RemoteException;

import chat.support.Group;
import chat.support.Message;
import chat.support.Status;


public interface IClientIM{

	// Transmit chat message
	public abstract void transmit(Message message) throws RemoteException;

	// Chat client configuration
	public abstract void configure(Group group) throws RemoteException;

	// Main client thread
	public abstract void run();

	// Initialize client
	public abstract boolean initialize(String sServer, String sName);

	// Attach to server
	public abstract boolean attach();

	// Detach from server
	public abstract boolean detach();

	// Send chat message
	public abstract void send(String sContents);

	// Request server status
	public abstract Status dump();

	// Abort client operation
	public abstract void abort();

	// Return number of messages
	public abstract int number();

}