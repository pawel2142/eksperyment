package chat.support;

import java.io.Serializable;

public class ClientPawelScenariusz3 extends Client implements Serializable{
	
	private static final long serialVersionUID = 3L;
	
	protected String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
	