/**
 * 
 */
package chat.support;

import java.io.Serializable;

public class Message implements Serializable{
	private static final long serialVersionUID = 1L;
	protected int iIdentifier;     // unique identifier
	protected String sName;		 // chat name
	protected String sContent;    // message content
	public int getIIdentifier() {
		return iIdentifier;
	}
	public void setIIdentifier(int identifier) {
		iIdentifier = identifier;
	}
	public String getSName() {
		return sName;
	}
	public void setSName(String name) {
		sName = name;
	}
	public String getSContents() {
		return sContent;
	}
	public void setSContents(String contents) {
		sContent = contents;
	}
}