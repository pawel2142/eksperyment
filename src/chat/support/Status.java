/**
 * 
 */
package chat.support;

import java.io.Serializable;
import java.util.TreeMap;


public class Status implements Serializable{
	private static final long serialVersionUID = 1L;
	protected TreeMap<Integer, Group> groups;
	protected int iMessages;
	public TreeMap<Integer, Group> getGroups() {
		if(groups==null){
			groups=new TreeMap<Integer, Group>();
		}
		return groups;
	}
	public void setGroups(TreeMap<Integer, Group> groups) {
		this.groups = groups;
	}
	public int getIMessages() {
		return iMessages;
	}
	public void setIMessages(int messages) {
		iMessages = messages;
	}
}