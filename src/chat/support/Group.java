/**
 * 
 */
package chat.support;

import java.io.Serializable;
import java.util.TreeMap;


public class Group implements Serializable {
	private static final long serialVersionUID = 1L;
	protected Client oLeader;					   // group leader
	protected TreeMap<Integer, Client> oClients;  // member clients
	public Client getOLeader() {
		return oLeader;
	}
	public void setOLeader(Client leader) {
		oLeader = leader;
	}
	public TreeMap<Integer, Client> getOClients() {
		if(oClients==null) {
			oClients=new  TreeMap<Integer, Client>();
		}
		return oClients;
	}
}