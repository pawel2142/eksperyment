package chat.support;

import java.rmi.RemoteException;

public class UsedNameException extends RemoteException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsedNameException() {
		super();
	}

	public UsedNameException(String s) {
		super(s);
	}
}
