package chat.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.Map.Entry;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import chat.IServer;
import chat.IServerIM;
import chat.impl.StandardServerImpl;
import chat.impl.StandardServerImplPawelScenariusz2;
import chat.impl.StandardServerImplPawelScenariusz3;
import chat.support.Client;
import chat.support.Group;
import chat.support.Status;

public class ServerUserInterfacePawelScenariusz3 extends ServerUserInterface{
	
	// Initialize chat server
	@Override
	public void initialize(){
		// Create server object
		
		m_server = new StandardServerImplPawelScenariusz3(this);		
		
		// Export server
		try {
			if (!m_server.initialize(m_iGroupMaximum)){
				// Display dialog box
				JOptionPane.showMessageDialog(serverFrame,
											  "ChatServer unable to export remote service",
											  "ChatServer",
											  JOptionPane.WARNING_MESSAGE);
				System.exit(0);
			}
		} catch (HeadlessException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args){
		// Parse arguments
		if (args.length >= 1) m_iGroupMaximum = Integer.parseInt(args[0]); 		
			
		// Schedule job for dispatching thread
		javax.swing.SwingUtilities.invokeLater(new Runnable(){
			public void run() 
			{
				// Make sure we have nice window decorations
				JFrame.setDefaultLookAndFeelDecorated(true);

				// Create and initialize user interface
				ServerUserInterfacePawelScenariusz3 userInterface = new ServerUserInterfacePawelScenariusz3();

				// Initialize server
				userInterface.initialize();
			}
		});
	}
}
