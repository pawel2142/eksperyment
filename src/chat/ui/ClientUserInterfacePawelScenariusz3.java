package chat.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import chat.IClient;
import chat.IClientIM;
import chat.IClientIMPawelScenariusz3;
import chat.impl.StandardClientImpl;
import chat.impl.StandardClientImplPawelScenariusz3;
import chat.support.ClientPawelScenariusz3;
import chat.support.Encryption;
import chat.support.FileLogger;

public class ClientUserInterfacePawelScenariusz3 extends ClientUserInterfacePawelScenariusz2 {
	protected static String m_sPassword;

	public static void main(String[] args) {
		// Parse arguments
		if (args.length >= 1)
			m_sChatName = args[0];
		if (args.length >= 2)
			m_sServerName = args[1];
		if (args.length >= 3)
			m_sPassword = args[2];

		logger = new FileLogger(m_sChatName + ".log");

		// Schedule job for dispatching thread
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// Make sure we have nice window decorations
				JFrame.setDefaultLookAndFeelDecorated(true);

				// Create user interface
				ClientUserInterfacePawelScenariusz3 userInterface = new ClientUserInterfacePawelScenariusz3();

				// Initialize client
				userInterface.initialize();
			}
		});
	}

	// Initialize chat client
	public void initialize() {
		// Create client object
		m_client = new StandardClientImplPawelScenariusz3(this);

		// Get chat name
		if ((m_sChatName == null) || (m_sChatName.length() == 0)) {
			m_sChatName = getString("Chat name:");
		}

		// Get server name
		if ((m_sServerName == null) || (m_sServerName.length() == 0)) {
			m_sServerName = getString("Server name:");
		}

		// Initialize client
		if (((IClientIMPawelScenariusz3) m_client).initialize(m_sServerName, m_sChatName, m_sPassword)) {
			// Attach to server
			if (((IClientIMPawelScenariusz3) m_client).attach()) {
				// Display the window.
				clientFrame.pack();
				clientFrame.setVisible(true);
				return;
			}
		}

		// Warning dialog and exit
		if (m_sServerName == null) {
			putString("Invalid chat server selected");
		} else {
			putString("Cannot connect to chat server " + m_sServerName);
		}
		System.exit(0);
	}
}
