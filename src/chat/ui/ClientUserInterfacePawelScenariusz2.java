package chat.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import chat.IClient;
import chat.IClientIM;
import chat.impl.StandardClientImpl;
import chat.support.Encryption;
import chat.support.FileLogger;


public class ClientUserInterfacePawelScenariusz2 extends ClientUserInterfacePawel {	
	
	protected static FileLogger logger;	

	// Client is sending a message
	@Override
	public void receive(String name, String contents){
		// Display message
		contents = Encryption.decrypt(contents);
		
		String displayedText = name + "> " + contents + System.lineSeparator();
		
		logger.log(displayedText);
		outputText.append(displayedText);
		
		// Scroll to bottom
		outputText.setCaretPosition(outputText.getDocument().getLength());
	}	

	//
	// Main entry point for client
	//
	public static void main(String[] args) {
		// Parse arguments
		if (args.length >= 1) m_sChatName = args[0]; 
		if (args.length >= 2) m_sServerName = args[1]; 
		
		logger = new FileLogger(m_sChatName + ".log");

		// Schedule job for dispatching thread
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// Make sure we have nice window decorations
				JFrame.setDefaultLookAndFeelDecorated(true);

				// Create user interface
				ClientUserInterfacePawelScenariusz2 userInterface = new ClientUserInterfacePawelScenariusz2();
				
				// Initialize client
				userInterface.initialize();
			}
		});
	}
}
