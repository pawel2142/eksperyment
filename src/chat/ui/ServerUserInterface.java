package chat.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.Map.Entry;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import chat.IServer;
import chat.IServerIM;
import chat.impl.StandardServerImpl;
import chat.support.Client;
import chat.support.Group;
import chat.support.Status;

public class ServerUserInterface implements ActionListener, IServer{
	// Maximum size argument
	protected static int m_iGroupMaximum=2;	

	// Chat server instance
	protected IServerIM m_server;		

	// User interface variables
	protected JFrame serverFrame;			
	protected JPanel serverPanel;
	protected JLabel logoImage;
	protected JLabel labelOutput;
	protected JTextArea outputText;
	protected JScrollPane outputScroll;
	protected JLabel labelGroup;
	protected JTextArea outputGroups;
	protected JScrollPane groupScroll;
	protected JLabel labelMessages;
	protected JLabel labelClients;
	protected JLabel labelGroups;
	protected JLabel labelSize;
	protected JLabel labelAverage;
	protected JButton viewButton;
	protected JButton quitButton;
	
	// Create server user interface
	public ServerUserInterface(){
		// Create window
		serverFrame = new JFrame("Chat server");

		// Initialize window
		serverFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		serverFrame.setSize(new Dimension(600,500));

		// Create panel
		serverPanel = new JPanel();
		serverPanel.setLayout(new GridBagLayout());

		// Create widgets
		ImageIcon imageIcon = new ImageIcon("images/serverlogo.gif");
		logoImage = new JLabel(imageIcon);
		outputText = new JTextArea(10, 50);
		outputScroll = new JScrollPane(outputText);
		outputScroll.setPreferredSize(new Dimension(560, 300));
		outputGroups = new JTextArea(10, 50);
		groupScroll= new JScrollPane(outputGroups);
		groupScroll.setPreferredSize(new Dimension(560, 100));
		labelMessages = new JLabel("", SwingConstants.LEFT);
		labelClients = new JLabel(":", SwingConstants.LEFT);
		labelGroups = new JLabel("", SwingConstants.LEFT);
		labelSize = new JLabel("", SwingConstants.LEFT);
		labelAverage = new JLabel("", SwingConstants.LEFT);
		viewButton = new JButton("View");
		quitButton = new JButton("Quit");
		labelOutput = new JLabel("Client List:", SwingConstants.LEFT);
		labelGroup = new JLabel("Chat Groups:", SwingConstants.LEFT);

		// Add widgets
		GridBagConstraints constraint = new GridBagConstraints();
		constraint.fill = GridBagConstraints.HORIZONTAL;
		constraint.weightx = 0.5;
		constraint.gridx = 0;
		constraint.gridy = 0;
		constraint.gridwidth = 2;
		constraint.insets.set(5,20,0,20);
		serverPanel.add(logoImage, constraint);
		constraint.gridx = 0;
		constraint.gridy = 1;
		constraint.ipady = 0; 
		serverPanel.add(labelOutput, constraint);
		constraint.gridx = 0;
		constraint.gridy = 2;
		serverPanel.add(outputScroll, constraint);
		constraint.gridx = 0;
		constraint.gridy = 3;
		serverPanel.add(labelGroup, constraint);
		constraint.gridx = 0;
		constraint.gridy = 4;
		serverPanel.add(groupScroll, constraint);
		constraint.gridx = 0;
		constraint.gridy = 5;
		serverPanel.add(labelMessages, constraint);
		constraint.gridx = 0;
		constraint.gridy = 6;
		serverPanel.add(labelClients, constraint);
		constraint.gridx = 0;
		constraint.gridy = 7;
		serverPanel.add(labelGroups, constraint);
		constraint.gridx = 0;
		constraint.gridy = 8;
		serverPanel.add(labelSize, constraint);
		constraint.gridx = 0;
		constraint.gridy = 9;
		serverPanel.add(labelAverage, constraint);
		constraint.gridx = 0;
		constraint.gridy = 10;
		constraint.gridwidth = 1;
		constraint.insets.set(5,80,5,80);
		serverPanel.add(viewButton, constraint);
		constraint.gridx = 1;
		constraint.gridy = 10;
		serverPanel.add(quitButton, constraint);

		// Add panels
		serverFrame.getContentPane().add(serverPanel);

		// Set font attributes
		Font font = new Font("Arial", Font.BOLD, 12);
		outputText.setForeground(new Color(64, 0, 255));
		outputText.setFont(font);
		outputGroups.setForeground(new Color(64, 0, 255));
		outputGroups.setFont(font);

		// Cannot focus on output
		outputText.setFocusable(false);

		// Listen to events
		viewButton.addActionListener(this);
		quitButton.addActionListener(this);

		// Initial values
		update(0,0,0,0);

		// Display the window
		serverFrame.pack();
		serverFrame.setVisible(true);
	}

	// Handler for button and keyboard input
	public void actionPerformed(ActionEvent event){
		if (event.getActionCommand().equals("Quit")){
			// Destroy server
			try {
				m_server.shutdown();
			} catch (RemoteException e) {
				e.printStackTrace();
			}

	    	System.exit(0);
		}
		else if (event.getActionCommand().equals("View")){
			// Display groups from server
			Status status=null;
			try {
				status = m_server._dump();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			String string = "";
		
			// Iterate groups
			Iterator<Entry<Integer, Group>> itGroup = status.getGroups().entrySet().iterator();
			while (itGroup.hasNext()){
				Group group = itGroup.next().getValue();
				string += group.getOLeader().getSName() + ": ";
				
		    	// Iterate clients
		    	Iterator<Entry<Integer, Client>> itClient = group.getOClients().entrySet().iterator();
		    	while (itClient.hasNext()) {
		    		Client client = itClient.next().getValue();
		    		string += client.getSName() + " ";
		    	}
		    	string += System.lineSeparator();
			}
			outputGroups.setText(string);
		}
	}

	// Initialize chat server
	public void initialize(){
		// Create server object
		
		m_server = new StandardServerImpl(this);
		
		
		// Export server
		try {
			if (!m_server.initialize(m_iGroupMaximum)){
				// Display dialog box
				JOptionPane.showMessageDialog(serverFrame,
											  "ChatServer unable to export remote service",
											  "ChatServer",
											  JOptionPane.WARNING_MESSAGE);
				System.exit(0);
			}
		} catch (HeadlessException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	// Update clients from chat server
	public void message(String sMessage){
		// Display message
		outputText.append(sMessage + "\n");

		// Scroll to bottom
		outputText.setCaretPosition(outputText.getDocument().getLength());
	}

	// Update statistics from chat server
	public void update(int clients, int groups, int messages, int size){
		float fAverage;
		
		if (groups == 0)
			fAverage = 0.0f;
		else
			fAverage = (float)clients / (float)groups;

		labelMessages.setText("Number of Messages: " + messages);
		labelClients.setText("Number of Clients: " + clients);
		labelGroups.setText("Number of Groups: " + groups);
		labelSize.setText("Maximum Size: " + size);
		labelAverage.setText("Average Size: " + String.format("%2.1f", fAverage));
	}

	//
	// Main entry point for server
	//
	public static void main(String[] args){
		// Parse arguments
		if (args.length >= 1) m_iGroupMaximum = Integer.parseInt(args[0]); 		
			
		// Schedule job for dispatching thread
		javax.swing.SwingUtilities.invokeLater(new Runnable(){
			public void run() 
			{
				// Make sure we have nice window decorations
				JFrame.setDefaultLookAndFeelDecorated(true);

				// Create and initialize user interface
				ServerUserInterface userInterface = new ServerUserInterface();

				// Initialize server
				userInterface.initialize();
			}
		});
	}
}
