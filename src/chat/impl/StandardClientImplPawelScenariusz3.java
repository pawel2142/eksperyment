package chat.impl;

import java.net.InetAddress;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentLinkedQueue;

import chat.IClient;
import chat.IClientIM;
import chat.IClientIMPawelScenariusz3;
import chat.IServerIM;
import chat.support.Callback;
import chat.support.Client;
import chat.support.ClientPawelScenariusz3;
import chat.support.Group;
import chat.support.Message;
import chat.support.Status;

public class StandardClientImplPawelScenariusz3 extends StandardClientImpl implements IClientIMPawelScenariusz3 {

	// Client constructor
	public StandardClientImplPawelScenariusz3(IClient clientInterface) {
		super(clientInterface);
		// Store user interface callback
		m_ui = clientInterface;
		m_client = new ClientPawelScenariusz3();
		// Create containers
		m_queueCommands = new ConcurrentLinkedQueue<Command>();
	}


	public boolean initialize(String sServer, String sName, String sPassword) {
		// Create and install a security manager
		if (System.getSecurityManager() == null)
			System.setSecurityManager(new SecurityManager());

		try {
			// Define service name
			String name = "ChatServer";

			// Locate rmi registry
			Registry registry = LocateRegistry.getRegistry(sServer);

			// Attach to remote object
			m_remote = (IServerIM) registry.lookup(name);

			// Export remote methods
			UnicastRemoteObject.exportObject(this, 0);

			// Setup client

			m_client.setSHost(InetAddress.getLocalHost().getHostName());
			m_client.setSAddress(InetAddress.getLocalHost().getHostAddress());
			m_client.setSName(sName);
			m_client.setOCallback(this);
			((ClientPawelScenariusz3) m_client).setPassword(sPassword);

			// Start client thread
			this.start();
			return true;
		} catch (RemoteException e) {
			e.printStackTrace();
			System.out.println("Chat server not available!");
			System.out.println(e.getMessage());
			System.exit(-1);
			return false;
		} catch (Exception e) {
			System.err.println(e);
			return false;
		}
	}

}
