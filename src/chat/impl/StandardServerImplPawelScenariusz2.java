package chat.impl;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentLinkedQueue;

import chat.IServer;
import chat.IServerIM;
import chat.support.Client;
import chat.support.FileLogger;
import chat.support.Group;
import chat.support.Message;
import chat.support.Status;
import chat.support.UsedNameException;

// Chat server implementation
public class StandardServerImplPawelScenariusz2 extends StandardServerImpl {
	
	private static final String SERVER_LOG_FILENAME = "server.log";	
	protected static FileLogger serverLogger = new FileLogger(SERVER_LOG_FILENAME); 
	
	public StandardServerImplPawelScenariusz2(IServer serverInterface) {
		super(serverInterface);		
	}

	// Add to attach queue
    public synchronized int attach(Client client) throws RemoteException {
    	int retValue=-1;
    	if(!nameList.contains(client.getSName())){
			nameList.add(client.getSName());
	    	// Assign unique identifier
			client.setIIdentifier(m_iClientId++);
	    	
	    	Command command = new Command();
	    	command.attach = client;
	    	m_queueCommands.add(command);		
			retValue=client.getIIdentifier();
			
			String joiningText = client.getSName() + " has joined";
			System.out.println(joiningText);
			serverLogger.log(joiningText);			
		}
		else {
			throw new UsedNameException("username in use: "+client.getSName());
		}  	
    	notify(); 	
    	return retValue;
    }

    // Add to detach queue
    public synchronized void detach(Client client) throws RemoteException{
		Command command = new Command();
    	command.detach = client;
    	m_queueCommands.add(command);
    	nameList.remove(client.getSName());
    	
    	String leavingText = client.getSName() + " has left";
    	System.out.println(leavingText);
    	serverLogger.log(leavingText);    	
    	
    	notify();
    }

	
}

