package chat.impl;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentLinkedQueue;

import chat.IServer;
import chat.IServerIM;
import chat.support.Client;
import chat.support.ClientPawelScenariusz3;
import chat.support.CredentialsLoader;
import chat.support.FileLogger;
import chat.support.Group;
import chat.support.Message;
import chat.support.Status;
import chat.support.UsedNameException;

// Chat server implementation
public class StandardServerImplPawelScenariusz3 extends StandardServerImplPawelScenariusz2 {

	public StandardServerImplPawelScenariusz3(IServer serverInterface) {
		super(serverInterface);
	}

	// Add to attach queue
	public synchronized int attach(Client client) throws RemoteException {
		if (isAuthentified((ClientPawelScenariusz3) client)) {
			return super.attach(client);
		} else {
			throw new RemoteException("Password is invalid. Sorry");
		}
	}

	protected boolean isAuthentified(ClientPawelScenariusz3 client) {
		boolean authentified = false;

		Map<String, String> credentials = CredentialsLoader.getCredentials();
		if (isClientRegistered(client, credentials) && isPasswordMatching(client, credentials)) {
			authentified = true;
		}

		return authentified;
	}

	protected boolean isClientRegistered(ClientPawelScenariusz3 client, Map<String, String> credentials) {
		return credentials.get(client.getSName()) != null;
	}

	protected boolean isPasswordMatching(ClientPawelScenariusz3 client, Map<String, String> credentials) {
		return credentials.get(client.getSName()).equals(client.getPassword());
	}

}
